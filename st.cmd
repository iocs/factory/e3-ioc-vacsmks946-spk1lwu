require essioc
require vac_ctrl_mks946_937b

iocshLoad("${essioc_DIR}/common_config.iocsh")
epicsEnvSet("MOXA_HOSTNAME", "moxa-vac-dtl-30-u006.tn.esss.lu.se")

epicsEnvSet("DEVICE_NAME", "Spk-010LWU:Vac-VEG-10001")
epicsEnvSet("MOXA_PORT", "4003")
epicsEnvSet("SN_A", "1906280619")
epicsEnvSet("SN_B", "1908061518")
epicsEnvSet("SN_C", "1908070848")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = $(DEVICE_NAME), BOARD_A_SERIAL_NUMBER = $(SN_A=9999999909), BOARD_B_SERIAL_NUMBER = $(SN_B=9999999909), BOARD_C_SERIAL_NUMBER = $(SN_C=9999999909), IPADDR = $(MOXA_HOSTNAME), PORT = $(MOXA_PORT)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGP-10000, CHANNEL = A1, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGP-10000, RELAY = 1, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGP-10000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGC-10000, CHANNEL = B1, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGC-10000, RELAY = 1, RELAY_DESC = 'Interlock PLC: VVS &amp; VVF Itlck'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGC-10000, RELAY = 2, RELAY_DESC = 'Process PLC: VPN Pressure Itlck'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGC-10000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010LWU:Vac-VGC-10000, RELAY = 4, RELAY_DESC = 'not wired'")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGP-10000, CHANNEL = A2, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGP-10000, RELAY = 1, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGP-10000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGC-10000, CHANNEL = C1, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGC-10000, RELAY = 1, RELAY_DESC = 'Interlock PLC: VVS Itlck'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGC-10000, RELAY = 2, RELAY_DESC = 'Process PLC: VPN Pressure Itlck'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGC-10000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020LWU:Vac-VGC-10000, RELAY = 4, RELAY_DESC = 'not wired'")

